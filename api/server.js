require('dotenv').config();

const express       = require('express');
const MongoClient   = require('mongodb').MongoClient;
const bodyParser    = require('body-parser');
const config        = require('./config');

const client = new MongoClient(config.db.url, { useNewUrlParser: true });

const app = express();
const port = (process.env.PORT || 8000);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ extended: true }));
app.strings = config.strings;
app.lib = require('./lib');

client.connect(err => {
    if (err) return console.log(err)

    db = client.db("bey-db");

    require('./app/routes')(app, db);

    app.listen(port, () => {
        console.log("API running on port " + port);
    });
})
