let ObjectID = require('mongodb').ObjectID;
let coll = 'lyrics'

module.exports = function(app, db) {
    let stdResponse = app.lib.response;

    app.get('/lyrics', (req, res) => {
        let s = {}
        db.collection(coll).find(s).toArray((err, items) => {
            if (err) {
                stdResponse(res, err);
            } else {
                stdResponse(res, err, items)
            }
        });
    });

    app.get('/lyrics/:id', (req, res) => {
        const id = req.params.id;
        const s = {'_id': new ObjectID(id)};
        db.collection(coll).findOne(s, (err, item) => {
            if (err) {
                stdResponse(res, err);
            } else {
                if (item) {
                    stdResponse(res, err, item)
                } else {
                    stdResponse(res, err, 'Item not found', app.strings.notfound(), 404)
                }
            }
        });
    });

    app.delete('/lyrics/:id', (req, res) => {
        const id = req.params.id;
        const s = {'_id': new ObjectID(id)};
        db.collection(coll).deleteOne(s, (err, item) => {
            if (err) {
                stdResponse(res, err);
            } else {
                stdResponse(res, err, 'Note ' + id + ' deleted!', "Boy bye")
            }
        });
    });

    app.post('/lyric',  (req, res) => {
        let lyric = {
            text: req.body.text,
            song: req.body.song
        }

        db.collection(coll).insertOne(lyric, (err, result) => {
            if (err) {
                stdResponse(res, err);
            } else {
                stdResponse(res, err, result.ops[0])
            }
            
        })
    });
};