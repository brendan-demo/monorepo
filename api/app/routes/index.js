const lyricRoutes = require('./lyric_routes');

module.exports = function(app, db) {
    lyricRoutes(app, db);
}