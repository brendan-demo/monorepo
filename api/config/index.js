const dbConfig = require('./db');
const stringConfig = require('./strings')

module.exports = {
    db: dbConfig,
    strings: stringConfig
}