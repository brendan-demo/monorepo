const dbusr = process.env.DBUSER
const dbpwd = process.env.DBPASSWORD

module.exports = {
   url: 'mongodb+srv://' + dbusr + ':' + dbpwd + '@cluster0-gdxpl.mongodb.net/test?retryWrites=true'
}