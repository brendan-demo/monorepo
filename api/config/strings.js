let twohundred = [
    "Chuckin' my deuces up",
    "YAS queen Bey!",
    "Imma keep runnin' cause a winner don't quit on themselves.",
    "I look so damn good, I ain't lost it.",
    "I woke up like this",
    "Some call it arrogant. I call it confident."
]

let fivehundred = [
    "Sorry, I ain't sorry",
    "You ain't never seen a fire like the one I'ma cause.",
    "I sneezed on the beat and the beat got sicker.",
    "Middle fingers up, put them hands high.",
    "Boy, bye",
    "If you liked it then you should've put a ring on it.",
    "Perfection is the disease of a nation.",
    "Hold up, they don't love you like I love you"
]

let fourohfour = [
    "Is there sumething that I'm missing?  Maybe my head for one..."
]


module.exports = {
    success: () => {
        return twohundred[Math.floor(Math.random()*twohundred.length)];
    },
    mybad: () => {
        return fivehundred[Math.floor(Math.random()*fivehundred.length)];
    },
    notfound: () => {
        return fourohfour[Math.floor(Math.random()*fourohfour.length)];
    }
}