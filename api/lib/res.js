const strings = require('../config/strings');


module.exports = function(res, err, details, strOverride = null, status = 200) {
    let error   = strings.mybad();
    let success = strings.success();

    if (strOverride) {
        error   = strOverride;
        success = strOverride;
    }

    if (err) {
        res.status(500).send({
            status: "error",
            message: error, 
            details: err.toString()
        });
    } else {
        res.status(status).send({ 
            status: "success",
            message: success,
            details: details
        });
    }
}